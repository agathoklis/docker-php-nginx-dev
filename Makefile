SHELL='/bin/bash'

server-up:
	docker-compose \
			--file .docker/docker-compose.yml \
			--project-directory . \
			up
